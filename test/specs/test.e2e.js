const { expect } = require('@wdio/globals');
const LoginPage = require('../pageobjects/login.page');
const MyAccount = require('../pageobjects/my-account.page');

describe('@UC-1', () => {
  it('Test Login form with empty credentials', async () => {
    await LoginPage.fillCredentials('user', 'password');
    await LoginPage.clearCredentials();

    await expect(await LoginPage.errorUsername).toHaveText(LoginPage.ERROR_USERNAME_MESSAGE);
    await expect(await LoginPage.errorPassword).toHaveText(LoginPage.ERROR_PASSWORD_MESSAGE);
  });
});

describe('@UC-2', () => {
  it('Test Login form with incorrect credentials', async () => {
    await LoginPage.fillCredentials('user', 'password');
    await LoginPage.submit();

    await expect(await LoginPage.errorCredentials).toHaveText(
      LoginPage.ERROR_INVALID_CREDS_MESSAGE,
    );
  });
});

describe('@UC-3', () => {
  it('Test Login form with correct credentials', async () => {
    await LoginPage.fillCredentials('roman.perun20@gmail.com', 'Aa123456');
    await LoginPage.submit();

    await expect(browser).toHaveUrl('https://accounts.spotify.com/en/status');
    await expect(MyAccount.accountName).toHaveText('Roman');
  });
});
