const { $ } = require('@wdio/globals');
const Page = require('../pageobjects/page');

class LoginPage extends Page {
  ERROR_USERNAME_MESSAGE = 'Please enter your Spotify username or email address.';
  ERROR_PASSWORD_MESSAGE = 'Please enter your password.';
  ERROR_INVALID_CREDS_MESSAGE = 'Incorrect username or password.';

  get inputUsername() {
    return $('#login-username');
  }

  get inputPassword() {
    return $('//input[@id="login-password"]');
  }

  get errorUsername() {
    return $('#username-error');
  }

  get errorPassword() {
    return $('#password-error');
  }

  get errorCredentials() {
    return $('.Message-sc-15vkh7g-0.dHbxKh');
  }

  get loginButton() {
    return $('#login-button');
  }

  async open() {
    await super.open('https://accounts.spotify.com/en/login');
  }

  async submit() {
    await this.loginButton.click();
  }

  async fillCredentials(username, password) {
    await this.open();
    await this.inputUsername.setValue(username);
    await this.inputPassword.setValue(password);
  }

  async clearCredentials() {
    const userNameValue = await this.inputUsername.getValue();
    const passwordValue = await this.inputPassword.getValue();

    await this.inputUsername.click();
    for (let i = 0; i < userNameValue.length; i++) {
      await browser.keys('Backspace');
    }

    await this.inputPassword.click();
    for (let i = 0; i < passwordValue.length; i++) {
      await browser.keys('Backspace');
    }
  }
}

module.exports = new LoginPage();
