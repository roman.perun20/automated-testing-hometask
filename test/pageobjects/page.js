const { browser } = require('@wdio/globals');

class Page {
  constructor() {
    this.title = 'Spotify page';
  }
  async open(path) {
    await browser.url(path);
  }
}

module.exports = Page;
