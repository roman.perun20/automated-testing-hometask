const Page = require('./page');

class MyAccount extends Page {
  get accountName() {
    return $('//div[normalize-space()=""]/following-sibling::div');
  }
}

module.exports = new MyAccount();
